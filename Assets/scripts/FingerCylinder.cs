﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerCylinder : MonoBehaviour {

    public GameObject Following;
    public GameObject FingerTip;
    public GameObject Cylinder;
    [Range(0.0f, 1.0f)]
    public float Interested;
	
	void Update ()
    {
        Vector3 dir = Following.transform.position - FingerTip.transform.position;
        Vector3 ndir = dir.normalized;
        float angle = (Mathf.Atan2(ndir.z, ndir.x) + Mathf.PI) * 0.5f / Mathf.PI;
        if (angle > 1.0f)
        {
            angle -= 1.0f;
        }
           
        angle *= -360;
        angle += 90;

        if (tag == "Left")
        {
            transform.rotation = Quaternion.Euler(90.0f, angle, 0.0f);
        }
            
        if(tag == "Right")
        {
            transform.rotation = Quaternion.Euler(90.0f, angle, 0.0f);
        }
    }

    void LateUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, Following.transform.position, Interested);
    }
}
 