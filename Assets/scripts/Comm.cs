﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class Comm : MonoBehaviour
{
    static public bool UseDAQ = false;
    public float MessageTime = 0.005f;
    private Process Proc;

    void Start()
    {
        var args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == "-daq")
            {
                UseDAQ = true;
                break;
            }
        }

        if (UseDAQ)
        {
            DebugLogger.Log("Using DAQ!");
            string daqPath;
            if (Application.isEditor)
            {
                string fullPath = Directory.GetCurrentDirectory();
                string parentDir = Directory.GetParent(fullPath).ToString();
                daqPath = parentDir + "\\DAQ-Writer.exe";
            }
            else
            {
                string fullpath = Directory.GetCurrentDirectory();
                daqPath = fullpath + "\\DAQ-Writer.exe";
            }

            Proc = new Process();
            Proc.StartInfo.FileName = daqPath;
            Proc.StartInfo.CreateNoWindow = true;
            Proc.StartInfo.UseShellExecute = false;
            Proc.StartInfo.RedirectStandardInput = true;
            Proc.StartInfo.RedirectStandardOutput = true;
            try
            {
                bool procSuccess = Proc.Start();
                if (!procSuccess)
                {
                    DebugLogger.Log("failed to open process");
                }

            }
            catch (System.Exception e)
            {
                DebugLogger.Log(e.Message);
            }
        }
    }

    public void SendMessageToDaq()
    {
        SendZero();
        Invoke("SendOne", MessageTime);
    }

    private void OnDisable()
    {
        if (UseDAQ)
        {
            Proc.StandardInput.WriteLine("2");
            Proc.Kill();
        }
    }

    private void SendZero()
    {
        Proc.StandardInput.WriteLine("0");
    }

    private void SendOne()
    {
        Proc.StandardInput.WriteLine("1");
    }
}
