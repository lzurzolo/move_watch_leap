﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationManager : MonoBehaviour
{

    private float ExitWaitTime;
    private float TimeLeftUntilExit;
    public bool IsPreparingToExit;

    // Use this for initialization
    void Start()
    {
        ExitWaitTime = 5.0f;
        TimeLeftUntilExit = 0.0f;
        IsPreparingToExit = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape"))
        {
            if (!Application.isEditor)
                Application.Quit();
        }

        if (IsPreparingToExit)
        {
            TimeLeftUntilExit += Time.deltaTime;
            if (TimeLeftUntilExit > ExitWaitTime)
            {
                if (!Application.isEditor)
                    Application.Quit();
            }
        }
    }
}
