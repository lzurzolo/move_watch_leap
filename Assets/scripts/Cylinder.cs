﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

public class Cylinder : MonoBehaviour
{
    public GameObject following;
    public GameObject cylinder;
    [Range(0.0f, 1.0f)]
    public float interested;

    bool visibility = true;

    private GameObject currentFollowing;

    void Start()
    {
        currentFollowing = following;
        transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
    }

    void Update()
    {
        cylinder.GetComponent<Renderer>().enabled = visibility;
    }

    void LateUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, currentFollowing.transform.position, interested);
    }

    public void ApplyRotation(float degrees)
    {
        if (tag == "Left")
        {
            transform.rotation = Quaternion.Euler(90.0f, degrees, 0.0f);
        }

        if (tag == "Right")
        {
            transform.rotation = Quaternion.Euler(90.0f, -degrees, 0.0f);
        }
    }

    public void SetInvisible()
    {
        visibility = false;
    }

    public void SetVisible()
    {
        visibility = true;
    }

    public void FollowOriginalHand()
    {
        currentFollowing = following;
    }
}
