﻿public enum EMirrorType
{
    E_NO,
    E_LEFT,
    E_RIGHT
};

[System.Serializable]
public class Prompt
{
    public string               WhichHand;
    public string               LastHand;
    public float                Angle;
    public float                RestTime;
    public float                PrepTime;
    public float                MoveTime;
    public bool                 Mirror;
    public bool                 LastMirror;
    public bool                 Observe;

    public EMirrorType          MirrorType;
    public EMirrorType          LastMirrorType;

    public void Init()
    {
        if (Mirror)
        {
            if (WhichHand == "Left")
            {
                MirrorType = EMirrorType.E_LEFT;
            }
            else if (WhichHand == "Right")
            {
                MirrorType = EMirrorType.E_RIGHT;
            }
            else
                MirrorType = EMirrorType.E_NO;
        }
        else
            MirrorType = EMirrorType.E_NO;

        if(LastMirror)
        {
            if (LastHand == "Left")
            {
                LastMirrorType = EMirrorType.E_LEFT;
            }
            else if (LastHand == "Right")
            {
                LastMirrorType = EMirrorType.E_RIGHT;
            }
            else
                LastMirrorType = EMirrorType.E_NO;
        }
    }
};