﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using System.IO;

public class DataWriter : MonoBehaviour
{
    private string DataFileName;
    private string EventFileName;
    private float Timer;
    public StreamWriter DataStreamWriter;
    public StreamWriter EventStreamWriter;
    private long Milliseconds;

    public Leap.Unity.LeapServiceProvider[] Providers;

    static public bool UseLogger = true;

    void Start()
    {
        if (UseLogger)
        {
            string[] args = System.Environment.GetCommandLineArgs();
            string logName = "";
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "--logFileName")
                {
                    logName = args[i + 1];
                    break;
                }
            }

            Timer = 0.0f;
            if(Application.isEditor)
            {
                DataFileName = string.Format(Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
                EventFileName = string.Format(Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
            }
            else
            {
                DataFileName = string.Format(logName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
                EventFileName = string.Format(logName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
            }

            DirectoryInfo di;
            if (Application.isEditor)
            {
                di = Directory.CreateDirectory("../log/");
            }
            else
            {
                di = Directory.CreateDirectory("log/");
            }

            // check if there are any log files in the log directory from today
            string[] dataFiles = Directory.GetFiles(di.FullName, Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyy") + "*", SearchOption.TopDirectoryOnly);
            string[] eventFiles = Directory.GetFiles(di.FullName, Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyy") + "*", SearchOption.TopDirectoryOnly);

            // get the count of the log files
            int dataCount = dataFiles.Length;
            int eventCount = eventFiles.Length;

            // if there are no data log files from today use the default name

            if (dataCount == 0)
            {
                if (Application.isEditor)
                {
                    DataFileName = string.Format(Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
                }
                else
                {
                    DataFileName = string.Format(logName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
                }
            }
            else // otherwise append the count to the end of the file name to prevent overwriting
            {
                if (Application.isEditor)
                {
                    DataFileName = string.Format(Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + dataCount + ").txt");
                }
                else
                {
                    DataFileName = string.Format(logName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + dataCount + ").txt");
                }
            }

            // if there are no event log files from today use the default name
            
            if (eventCount == 0)
            {
                if (Application.isEditor)
                {
                    EventFileName = string.Format(Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
                }
                else
                {
                    EventFileName = string.Format(logName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
                }
            }
            else // otherwise append the count to the end of the file name to prevent overwriting
            {
                if (Application.isEditor)
                {
                    EventFileName = string.Format(Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + eventCount + ").txt");
                }
                else
                {
                    EventFileName = string.Format(logName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + eventCount + ").txt");
                }
            }
            
            DataStreamWriter = new StreamWriter(di.FullName + DataFileName, true);
            EventStreamWriter = new StreamWriter(di.FullName + EventFileName, true);

            EventStreamWriter.Write("time,ms,trialState,trialHands,leftRayStartPointX,leftRayStartPointY,leftRayStartPointZ,leftRayEndPointX,leftRayEndPointY,leftRayEndPointZ,rightRayStartPointX,rightRayStartPointY," +
                "rightRayStartPointZ,rightRayEndPointX,rightRayEndPointY,rightRayEndPointZ\n");

            DataStreamWriter.Write(
                "time,ms,hand,palmPosX,palmPosY,palmPosZ,palmNormX,palmNormY,palmNormZ,wristPosX,wristPosY,wristPosZ," +
                "thumbMetaCarpalX,thumbMetaCarpalY,thumbMetaCarpalZ,thumbProximalX,thumbProximalY,thumbProximalZ,thumbIntermediateX,thumbInterMediateY,thumbInterMediateZ,thumbDistalX,thumbdistalY,thumbDistalZ,thumbTipX,thumbTipY,thumbTipZ," +
                "indexMetaCarpalX,indexMetaCarpalY,indexMetaCarpalZ,indexProximalX,indexProximalY,indexProximalZ,indexIntermediateX,indexInterMediateY,indexInterMediateZ,indexDistalX,indexdistalY,indexDistalZ,indexTipX,indexTipY,indexTipZ," +
                "middleMetaCarpalX,middleMetaCarpalY,middleMetaCarpalZ,middleProximalX,middleProximalY,middleProximalZ,middleIntermediateX,middleInterMediateY,middleInterMediateZ,middleDistalX,middledistalY,middleDistalZ,middleTipX,middleTipY,middleTipZ," +
                "ringMetaCarpalX,ringMetaCarpalY,ringMetaCarpalZ,ringProximalX,ringProximalY,ringProximalZ,ringIntermediateX,ringInterMediateY,ringInterMediateZ,ringDistalX,ringdistalY,ringDistalZ,ringTipX,ringTipY,ringTipZ," +
                "pinkyMetaCarpalX,pinkyMetaCarpalY,pinkyMetaCarpalZ,pinkyProximalX,pinkyProximalY,pinkyProximalZ,pinkyIntermediateX,pinkyInterMediateY,pinkyInterMediateZ,pinkyDistalX,pinkydistalY,pinkyDistalZ,pinkyTipX,pinkyTipY,pinkyTipZ\n");
        }
    }

    public void LogData()
    {

        foreach (var p in Providers)
        {
            Frame f = p.CurrentFrame;
            if (f.Hands.Count == 1)
            {
                System.TimeSpan timeSpan;
                Timer += Time.deltaTime;
                timeSpan = System.TimeSpan.FromSeconds(Timer);

                Milliseconds = timeSpan.Minutes * 60000 + timeSpan.Seconds * 1000 + timeSpan.Milliseconds;
                DataStreamWriter.Write(System.DateTime.Now.ToString("h:mm::ss tt "));
                DataStreamWriter.Write(Milliseconds);

                if (f.Hands[0].IsLeft)
                {
                    DataStreamWriter.Write(",left");
                }
                if (f.Hands[0].IsRight)
                {
                    DataStreamWriter.Write(",right");
                }

                Hand hand = f.Hands[0];
                DataStreamWriter.Write("," + hand.PalmPosition.x);
                DataStreamWriter.Write("," + hand.PalmPosition.y);
                DataStreamWriter.Write("," + hand.PalmPosition.z);

                DataStreamWriter.Write("," + hand.PalmNormal.x);
                DataStreamWriter.Write("," + hand.PalmNormal.y);
                DataStreamWriter.Write("," + hand.PalmNormal.z);

                DataStreamWriter.Write("," + hand.WristPosition.x);
                DataStreamWriter.Write("," + hand.WristPosition.y);
                DataStreamWriter.Write("," + hand.WristPosition.z);

                foreach (Finger finger in hand.Fingers)
                {

                    Bone bone;
                    foreach (Bone.BoneType boneType in (Bone.BoneType[])System.Enum.GetValues(typeof(Bone.BoneType)))
                    {

                        if (boneType.ToString() != "TYPE_INVALID")
                        {
                            bone = finger.Bone(boneType);

                            DataStreamWriter.Write("," + bone.PrevJoint.x);
                            DataStreamWriter.Write("," + bone.PrevJoint.y);
                            DataStreamWriter.Write("," + bone.PrevJoint.z);
                        }
                    }
                    DataStreamWriter.Write("," + finger.TipPosition.x);
                    DataStreamWriter.Write("," + finger.TipPosition.y);
                    DataStreamWriter.Write("," + finger.TipPosition.z);
                }
                DataStreamWriter.Write("\n");
            }
        }
    }

    private void OnDestroy()
    {
        if (UseLogger)
        {
            EventStreamWriter.Close();
            DataStreamWriter.Close();
        }
    }

    public void LogCustomData(string input)
    {
        EventStreamWriter.Write(System.DateTime.Now.ToString("h:mm::ss tt "));
        EventStreamWriter.Write(Milliseconds);
        EventStreamWriter.Write("," + input);
        EventStreamWriter.Write(System.DateTime.Today.ToString("," + "MM/dd/yyyy"));
        EventStreamWriter.Write("\n");
    }
}
