﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using UnityEngine.UI;
using Leap;
using System;


public class MovementManager : MonoBehaviour
{
    [Header("Hand")]
    public Transform                            LeftIndexFingerPoint;
    public Transform                            LeftIndexFingerKnuckle;
    public Transform                            RightIndexFingerPoint;
    public Transform                            RightIndexFingerKnuckle;
    public GameObject                           LeftHand;
    public GameObject                           RightHand;
    public Transform                            LeftHandContainer;
    public Transform                            RightHandContainer;
    public GameObject                           DummyLeftHand;
    public GameObject                           DummyRightHand;

    [Header("Leap Providers")]
    public Leap.Unity.LeapServiceProvider       LeftProvider;
    public Leap.Unity.LeapServiceProvider       RightProvider;
    public GameObject                           LeftDevice;
    public GameObject                           RightDevice;

    [Header("Ray")]
    public GameObject                           LeftRay;
    public GameObject                           RightRay;
    public GameObject                           LeftFingerRay;
    public GameObject                           RightFingerRay;
    private Cylinder                            LeftRayScript;
    private Cylinder                            RightRayScript;
    public Transform                            LeftRayEndPoint;
    public Transform                            RightRayEndPoint;

    [Header("HUD")]
    public Text                                 HUDMovementsLeft;
    public Text                                 HUDCurrentMove;
    public Text                                 HUDPrepareToMove;

    private int                                 NumberOfPromptsLeft;

    private Renderer                            LeftRayRenderer;
    private Renderer                            RightRayRenderer;
    private Renderer                            LeftFingerRayRenderer;
    private Renderer                            RightFingerRayRenderer;

    private List<Prompt>                        PromptList;
    private Prompt                              CurrentPrompt;
    private static short                        CurrentPromptIndex;

    private float                               CurrentTime;

    // Set this flag to test hand movement, this will prevent the simulation from running through movements
    static public bool                          HandDebugMode;
    private DataWriter                          Writer;
    private Comm                                DAQComm;
    private bool                                WasPrepMessageSent;

    [Header("Simulation Parameters")]
    public bool                                 UseInvisibleMode = false;

    public enum HandOptions
    {
        ENABLE_BOTH,
        DISABLE_LEFT,
        DISABLE_RIGHT
    };

    public HandOptions                          SelectedHandOption;

    private float                               TotalPromptDuration;
    private float                               UndoMirrorDuration;
    private float                               RestAndPrepDuration;
    private bool                                AppliedMirroring;
    private bool                                UndidMirroring;
    private bool                                SentRestDAQMessage;
    private bool                                SentPrepDAQMessage;
    private bool                                SentMoveDAQMessage;
    private bool                                LoggedRestPrompt;
    private bool                                LoggedPrepPrompt;
    private bool                                LoggedMovePrompt;

    void Start ()
    {
        Zero();
        ReadConfigFileToMovementList();
        CountNumberOfMovements();
        SetHUDNumberOfMovesLeft(NumberOfPromptsLeft);
        GetLoggingReferences();
        GetDAQReferences();
        GetRayReferences();

        HandDebugMode = false;

        var args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == "-invisible")
            {
                UseInvisibleMode = true;
                break;
            }
        }
        ConfigureHands();
    }
	
	void Update ()
    {
        if (!AreEnoughHandsVisible())
        {
            LeftRayScript.SetInvisible();
            RightRayScript.SetInvisible();
            LeftFingerRayRenderer.enabled = false;
            RightFingerRayRenderer.enabled = false;
            return;
        }
        else
        {
            LeftRayScript.SetVisible();
            RightRayScript.SetVisible();
            LeftFingerRayRenderer.enabled = true;
            RightFingerRayRenderer.enabled = true;
        }

        if (SelectedHandOption == HandOptions.DISABLE_LEFT)
        {
            LeftRayScript.SetInvisible();
            DummyLeftHand.SetActive(true);
            LeftHand.SetActive(false);
            LeftFingerRayRenderer.enabled = false;
        }
        else if (SelectedHandOption == HandOptions.DISABLE_RIGHT)
        {
            RightRayScript.SetInvisible();
            DummyRightHand.SetActive(true);
            RightHand.SetActive(false);
            RightFingerRayRenderer.enabled = false;
        }

        CurrentTime += Time.deltaTime;
        NewPromptUpdate();
        RestUpdate();
        PrepareUpdate();
        MoveUpdate();

        if (DataWriter.UseLogger)
            Writer.LogData();
    }

    private void StepToNextMovement()
    {
        CurrentPrompt = GetNextMovement();
        TotalPromptDuration = CurrentPrompt.RestTime + CurrentPrompt.PrepTime + CurrentPrompt.MoveTime;
        RestAndPrepDuration = CurrentPrompt.RestTime + CurrentPrompt.PrepTime;
        UndoMirrorDuration = CurrentPrompt.RestTime * 0.5f;
        CurrentPromptIndex++;
        NumberOfPromptsLeft--;
        if (Comm.UseDAQ)
        {
            DAQComm.SendMessageToDaq();
        }
    }

    private void Zero()
    {
        CurrentPromptIndex = 0;
        NumberOfPromptsLeft = 0;
        CurrentTime = 0.0f;
        AppliedMirroring = false;
        UndidMirroring = false;
        UndoMirrorDuration = 0.0f;
        SentRestDAQMessage = false;
        SentPrepDAQMessage = false;
        SentMoveDAQMessage = false;
        LoggedRestPrompt = false;
        LoggedPrepPrompt = false;
        LoggedMovePrompt = false;
}

    private void NewPromptUpdate()
    {
        if (CurrentTime > TotalPromptDuration)
        {
            if (CurrentPromptIndex < PromptList.Count)
            {
                StepToNextMovement();
                CurrentTime = 0.0f;
                SetHUDNumberOfMovesLeft(NumberOfPromptsLeft + 1);
                UnsetOneTimeSequenceFlags();
            }
            else
            {
                GameObject.Find("ApplicationManager").GetComponent<ApplicationManager>().IsPreparingToExit = true;
                HUDPrepareToMove.text = "Test Completed. Exiting...";
                HUDPrepareToMove.enabled = true;
                SetHUDCurrentMove("");
                LeftRayScript.SetInvisible();
                RightRayScript.SetInvisible();
                LeftFingerRayRenderer.enabled = false;
                RightFingerRayRenderer.enabled = false;
                SetHUDNumberOfMovesLeft(0);
            }
        }
    }

    private void RestUpdate()
    {
        if (CurrentTime < CurrentPrompt.RestTime)
        {
            if (Comm.UseDAQ && !SentRestDAQMessage)
            {
                DAQComm.SendMessageToDaq();
                SentRestDAQMessage = true;
            }

            if (!LoggedRestPrompt)
            {
                Log("Rest");
                LoggedRestPrompt = true;
            }
                
            LeftRayScript.ApplyRotation(0.0f);
            RightRayScript.ApplyRotation(0.0f);
            SetHUDCurrentMove("Rest");
            if (CurrentPrompt.LastMirror && !UndidMirroring && CurrentTime > UndoMirrorDuration)
            {
                if (CurrentPrompt.Mirror && CurrentPrompt.MirrorType == CurrentPrompt.LastMirrorType)
                {
                    // reserved for future use
                }
                else
                {
                    if (CurrentPrompt.LastMirrorType == EMirrorType.E_LEFT)
                    {
                        if(SelectedHandOption == HandOptions.DISABLE_RIGHT)
                        {
                            UndoMirroring(LeftDevice, LeftHand, RightDevice, DummyRightHand);
                        }
                        else
                        {
                            UndoMirroring(LeftDevice, LeftHand, RightDevice, RightHand);
                        }
                    }
                    else if (CurrentPrompt.LastMirrorType == EMirrorType.E_RIGHT)
                    {
                        if(SelectedHandOption == HandOptions.DISABLE_LEFT)
                        {
                            UndoMirroring(RightDevice, RightHand, LeftDevice, DummyLeftHand);
                        }
                        else
                        {
                            UndoMirroring(RightDevice, RightHand, LeftDevice, LeftHand);
                        }
                    }
                }
                UndidMirroring = true;
            }
        }
    }

    private void PrepareUpdate()
    {
        if (CurrentTime > CurrentPrompt.RestTime && CurrentTime < RestAndPrepDuration)
        {
            if (Comm.UseDAQ && !SentPrepDAQMessage)
            {
                DAQComm.SendMessageToDaq();
                SentPrepDAQMessage = true;
            }

            if (!LoggedPrepPrompt)
            {
                Log("Prep");
                LoggedPrepPrompt = true;
            }

            HUDPrepareToMove.enabled = true;
            if (CurrentPrompt.Mirror)
            {
                HUDPrepareToMove.text = "Prepare to move " + CurrentPrompt.WhichHand + " (MIRROR)";
            }
            else
            {
                HUDPrepareToMove.text = "Prepare to move " + CurrentPrompt.WhichHand;
            }
            if (Comm.UseDAQ && !WasPrepMessageSent)
            {
                DAQComm.SendMessageToDaq();
                WasPrepMessageSent = true;
            }
        }
    }

    private void MoveUpdate()
    {
        if (CurrentTime > RestAndPrepDuration && CurrentTime < TotalPromptDuration)
        {
            if (Comm.UseDAQ && !SentMoveDAQMessage)
            {
                DAQComm.SendMessageToDaq();
                SentMoveDAQMessage = true;
            }

            if (!LoggedMovePrompt)
            {
                Log("Move");
                LoggedMovePrompt = true;
            }

            HUDPrepareToMove.enabled = false;
            HUDPrepareToMove.text = "";
            if (CurrentPrompt.Mirror)
            {
                SetHUDCurrentMove("Move " + CurrentPrompt.WhichHand + " (Mirror)");
            }
            else
            {
                SetHUDCurrentMove("Move " + CurrentPrompt.WhichHand);
            }

            if (CurrentPrompt.WhichHand == "Left")
            {
                if (CurrentPrompt.Mirror)
                {
                    LeftRayScript.ApplyRotation(-CurrentPrompt.Angle);
                    RightRayScript.ApplyRotation(CurrentPrompt.Angle);
                }
                else
                {
                    LeftRayScript.ApplyRotation(CurrentPrompt.Angle);
                    RightRayScript.ApplyRotation(CurrentPrompt.Angle);
                }

                if (CurrentPrompt.Mirror && !AppliedMirroring)
                {

                    if (CurrentPrompt.LastMirror && CurrentPrompt.MirrorType == CurrentPrompt.LastMirrorType)
                    {
                        // reserved for future use
                    }
                    else
                    {
                        if(SelectedHandOption == HandOptions.DISABLE_RIGHT)
                        {
                            ApplyMirroring(LeftDevice, LeftHand, RightDevice, DummyRightHand);
                        }
                        else
                        {
                            ApplyMirroring(LeftDevice, LeftHand, RightDevice, RightHand);
                        }
                    }
                    AppliedMirroring = true;
                }
                RightRayScript.SetInvisible();
                LeftRayScript.SetVisible();
            }
            else if (CurrentPrompt.WhichHand == "Right")
            {
                if (CurrentPrompt.Mirror)
                {
                    LeftRayScript.ApplyRotation(CurrentPrompt.Angle);
                    RightRayScript.ApplyRotation(-CurrentPrompt.Angle);
                }
                else
                {
                    LeftRayScript.ApplyRotation(CurrentPrompt.Angle);
                    RightRayScript.ApplyRotation(CurrentPrompt.Angle);
                }
                if (CurrentPrompt.Mirror && !AppliedMirroring)
                {
                    if (CurrentPrompt.LastMirror && CurrentPrompt.MirrorType == CurrentPrompt.LastMirrorType)
                    {
                        // reserved for future use
                    }
                    else
                    {
                        if(SelectedHandOption == HandOptions.DISABLE_LEFT)
                        {
                            ApplyMirroring(RightDevice, RightHand, LeftDevice, DummyLeftHand);
                        }
                        else
                        {
                            ApplyMirroring(RightDevice, RightHand, LeftDevice, LeftHand);
                        }

                    }
                    AppliedMirroring = true;
                }
                RightRayScript.SetVisible();
                LeftRayScript.SetInvisible();
            }
            else
            {
                RightRayScript.SetVisible();
                LeftRayScript.SetVisible();
                LeftRayScript.ApplyRotation(CurrentPrompt.Angle);
                RightRayScript.ApplyRotation(CurrentPrompt.Angle);
            }
        }
    }

    /*
     * @brief Parse the json encoded file and populate the Movement List with the result
     */ 
    private void ReadConfigFileToMovementList()
    {
        string configFile = Path.Combine(Application.dataPath, "config.json");
        string movementData = File.ReadAllText(configFile);
        PromptList = JsonConvert.DeserializeObject<List<Prompt>>(movementData);

        foreach(var p in PromptList)
        {
            p.Init();
        }
    }

    /*
     * @brief Get the next movement from the movement list
     * @return Movement The next movement
     */
    public Prompt GetNextMovement()
    {
        return PromptList[CurrentPromptIndex];
    }

    /*
     * @brief Get a count of how many movements there are in the movement list
     */
    private void CountNumberOfMovements()
    {
        NumberOfPromptsLeft = PromptList.Count;
    }

    /*
     * @brief Set the HUD message to display either "Move" or "Rest"
     * @param currentMovement The name of the movement
     */
    public void SetHUDCurrentMove(string currentMovement)
    {
        HUDCurrentMove.text = currentMovement;
    }

    /*
     * @brief Set the number of "Move" instructions left on the HUD
     * @param movesLeft The number of instructions left
     */
    public void SetHUDNumberOfMovesLeft(int movesLeft)
    {
        HUDMovementsLeft.text = movesLeft.ToString(); 
    }

    /*
     * @brief Get the reference to the data writer
     */
    private void GetLoggingReferences()
    {
        if (DataWriter.UseLogger)
        {
            Writer = GameObject.Find("Data Writer").GetComponent<DataWriter>();
        }
    }

    /*
     * @brief Get the reference to the DAQ
     */
    private void GetDAQReferences()
    {
        if (Comm.UseDAQ)
        {
            DAQComm = GameObject.Find("DAQComm").GetComponent<Comm>();
        }
    }

    /*
     * @brief Get references to the rays
     */
    private void GetRayReferences()
    {
        LeftRayScript = LeftRay.GetComponent<Cylinder>();
        RightRayScript = RightRay.GetComponent<Cylinder>();

        RightRayRenderer = GameObject.Find("RightCylinder").GetComponent<Renderer>();
        LeftRayRenderer = GameObject.Find("LeftCylinder").GetComponent<Renderer>();
        RightFingerRayRenderer = GameObject.Find("RightFingerCylinder").GetComponent<Renderer>();
        LeftFingerRayRenderer = GameObject.Find("LeftFingerCylinder").GetComponent<Renderer>();
    }

    /*
     * @brief Check whether the correct amount of hands are visible. If using mirror with with invisibility this checks if only the mirrored hand is visible
     * @return False if the correct amount of hands are not visible. True if the correct amount of hands are visible
     */

    private bool AreEnoughHandsVisible()
    {
        if(UseInvisibleMode)
        {
            if (!LeftHand.activeInHierarchy || !RightHand.activeInHierarchy)
                return false;
            return true;
        }
        else
        {
            if(SelectedHandOption == HandOptions.ENABLE_BOTH)
            {
                if (!LeftHand.activeInHierarchy || !RightHand.activeInHierarchy)
                    return false;
                return true;
            }
            else if(SelectedHandOption == HandOptions.DISABLE_LEFT)
            {
                if (!DummyLeftHand.activeInHierarchy || !RightHand.activeInHierarchy)
                    return false;
                return true;
            }
            else if(SelectedHandOption == HandOptions.DISABLE_RIGHT)
            {
                if (!DummyRightHand.activeInHierarchy || !LeftHand.activeInHierarchy)
                    return false;
                return true;
            }
        }
        return true;
    }

    /*
     * @brief Apply mirror mode for this trial
     * @param mirrorDevice The device to mirror as per the respective movement
     * @param otherDevice The second device
     */
    public void ApplyMirroring(GameObject mirrorDevice, GameObject mirrorHand, GameObject otherDevice, GameObject otherHand)
    {
        Vector3 mirrorScale = new Vector3(-1.0f, -1.0f, -1.0f);
        mirrorDevice.transform.localScale = mirrorScale;
        if(mirrorDevice.tag == "Left")
        {
            mirrorDevice.transform.Rotate(Vector3.up, 180, Space.Self);
            otherDevice.transform.localScale = new Vector3(-1.0f, -1.0f, -1.0f);
            otherDevice.transform.Rotate(Vector3.up, -180, Space.Self);
        }
        else
        {
            mirrorDevice.transform.Rotate(Vector3.up, -180, Space.Self);
            otherDevice.transform.localScale = mirrorScale;
            otherDevice.transform.Rotate(Vector3.up, 180);
        }
        if(UseInvisibleMode)
            otherHand.SetActive(false);
    }

    /*
     * @brief Undo the mirroring of the previous trial
     * @param mirrorDevice The device that was mirrored in the previous trial
     * @param otherDevice The other device
     */
    private void UndoMirroring(GameObject mirrorDevice, GameObject mirrorHand, GameObject otherDevice, GameObject otherHand)
    {
        Vector3 BufferedLeftHandPosition = LeftHandContainer.position;
        Vector3 BufferedRightHandPosition = RightHandContainer.position;

        Vector3 normalScale = new Vector3(1, 1, 1);
        mirrorDevice.transform.localScale = normalScale;
        if (mirrorDevice.tag == "Left")
        {
            mirrorDevice.transform.Rotate(Vector3.up, -180, Space.Self);
            otherDevice.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            otherDevice.transform.Rotate(Vector3.up, 180, Space.Self);
        }
        else
        {
            mirrorDevice.transform.Rotate(Vector3.up, 180, Space.Self);
            otherDevice.transform.localScale = normalScale;
            otherDevice.transform.Rotate(Vector3.up, -180);
        }

        LeftHandContainer.position = BufferedRightHandPosition;
        RightHandContainer.position = BufferedLeftHandPosition;
        otherHand.SetActive(true);
        LeftRayScript.SetVisible();
        RightRayScript.SetVisible();
    }

    private void ConfigureHands()
    {
        if(!Application.isEditor)
        {
            var args = System.Environment.GetCommandLineArgs();
            string handOption = "";
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "--hands")
                {
                    handOption = args[i + 1];
                    break;
                }
            }

            if (handOption == "enabled")
            {
                SelectedHandOption = HandOptions.ENABLE_BOTH;
                return;
            }
                
            else if (handOption == "disableLeft")
            {
                DummyLeftHand.SetActive(true);
                LeftHand.SetActive(false);
                SelectedHandOption = HandOptions.DISABLE_LEFT;
            }
            else if (handOption == "disableRight")
            {
                DummyRightHand.SetActive(true);
                RightHand.SetActive(false);
                SelectedHandOption = HandOptions.DISABLE_RIGHT;
            }
        }
        else
        {
            if (SelectedHandOption == HandOptions.ENABLE_BOTH)
            {
                return;
            }

            else if(SelectedHandOption == HandOptions.DISABLE_LEFT)
            {
                DummyLeftHand.SetActive(true);
                LeftHand.SetActive(false);
            }
            else if(SelectedHandOption == HandOptions.DISABLE_RIGHT)
            {
                DummyRightHand.SetActive(true);
                RightHand.SetActive(false);
            }
        }
    }

    private void Log(string prompt)
    {
        Writer.LogCustomData(
            prompt +
            "," +
            CurrentPrompt.WhichHand +
            "," +
            LeftIndexFingerKnuckle.position.x +
            "," +
            LeftIndexFingerKnuckle.position.y +
            "," +
            LeftIndexFingerKnuckle.position.z +
            "," +
            LeftRayEndPoint.position.x +
            "," +
            LeftRayEndPoint.position.y +
            "," +
            LeftRayEndPoint.position.z +
            "," +
            RightIndexFingerKnuckle.position.x +
            "," +
            RightIndexFingerKnuckle.position.y +
            "," +
            RightIndexFingerKnuckle.position.z +
            "," +
            RightRayEndPoint.position.x +
            "," +
            RightRayEndPoint.position.y +
            "," +
            RightRayEndPoint.position.z);
    }

    private void UnsetOneTimeSequenceFlags()
    {
        AppliedMirroring = false;
        UndidMirroring = false;

        SentRestDAQMessage = false;
        SentPrepDAQMessage = false;
        SentMoveDAQMessage = false;

        LoggedPrepPrompt = false;
        LoggedRestPrompt = false;
        LoggedMovePrompt = false;
    }
}


