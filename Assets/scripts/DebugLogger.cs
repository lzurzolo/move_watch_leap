﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DebugLogger : MonoBehaviour
{
    public static string Path = "DebugLog.txt";

    public static void Log(string message)
    {
        using (StreamWriter writer = new StreamWriter(Path, true))
        {
            writer.WriteLine(message);
            writer.Close();
        }
    }
}
